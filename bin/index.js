#!/usr/bin/env node
'use strict'
const { writeFileSync } = require('fs')
const path = require('path')
const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const AJV = require('ajv')
const vsCode = require('ajv-vscode')
const formats = require('ajv-formats')
const { mkdir } = require('shelljs')
const { dump } = require('js-yaml')
const $RefParser = require('@apidevtools/json-schema-ref-parser')
const { schema } = require('@rdmo-author/schema')
const { write } = require('@rdmo-author/xml')
const { gitDescribeSync } = require('git-describe')

const ajv = vsCode(new AJV({
  formats: {
    uri: formats.get('uri'),
    'uri-reference': formats.get('uri-reference')
  }
}))
const validate = ajv.compile(schema)

const gitDescribeOptions = { match: ['[0-9]*', 'v[0-9]*'] }

yargs(hideBin(process.argv))
  .command('validate <project>', 'Validate your project', (yargs) => {
    yargs
      .positional('project', {
        description: 'The root file of your RDMO catalog'
      })
  }, async (argv) => {
    const data = await $RefParser.dereference(argv.project, { resolve: { http: false } })
    if (validate(data) === false) {
      console.log(ajv.errorsText(validate.errors, { separator: '\n', dataVar: 'catalog' }))
    } else {
      console.log('Your catalog is valid')
    }
  })
  .command('export <project>', 'Export your catalog to xml for being imported into RDMO', (yargs) => {
    yargs
      .positional('project', {
        description: 'The root file of your RDMO catalog'
      })
      .option('out', {
        alias: 'o',
        type: 'string',
        description: 'Output directory where to write the xml files',
        default: '.'
      })
      .option('gitVersion', {
        type: 'boolean',
        description: 'Try to infer the catalog version from its git repo via git describe',
        default: true
      })
  }, async (argv) => {
    const data = await $RefParser.dereference(argv.project, { resolve: { http: false } })
    if (argv.gitVersion === true) {
      const gitInfo = gitDescribeSync(path.resolve(argv.project, '..'), gitDescribeOptions)
      if (gitInfo.semverString) { data.version = gitInfo.semverString }
    }
    const xml = write(data)
    mkdir('-p', argv.out)
    Object.entries(xml)
      .forEach(([name, doc]) => writeFileSync(path.resolve(argv.out, `${name}.xml`), doc))
  })
  .command('bundle <project>', 'Bundle a catalog spread over multiple files into one single file', yargs => {
    yargs
      .positional('project', {
        description: 'The root file of your RDMO catalog'
      })
      .option('format', {
        alias: 'f',
        type: 'string',
        choices: ['json', 'yaml'],
        description: 'Output format',
        default: 'json'
      })
      .option('gitVersion', {
        type: 'boolean',
        description: 'Try to infer the catalog version from its git repo via git describe',
        default: true
      })
  }, async (argv) => {
    const data = await $RefParser.bundle(argv.project, { resolve: { http: false } })
    if (argv.gitVersion === true) {
      const gitInfo = gitDescribeSync(path.resolve(argv.project, '..'), gitDescribeOptions)
      if (gitInfo.semverString) { data.version = gitInfo.semverString }
    }
    if (argv.format === 'yaml') {
      await process.stdout.write(dump(data))
    } else {
      await process.stdout.write(JSON.stringify(data, null, 2))
    }
  })
  .parse()
