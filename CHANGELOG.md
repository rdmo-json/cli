# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Export additional `full.xml` file which contains the catalog in one XML document

## [0.5.1] - 2021-04-16

### Changed

- Updated deps

## [0.5.0] - 2021-03-10

### Changed

- Revert last changes because XML must be split up into endpoint-specific files.

## [0.4.0] - 2021-03-09

### Changed

- Update @rdmo-author/xml which writes catalog to one XML document

## [0.3.1] - 2021-03-08

- Update deps

## [0.3.0] - 2021-02-07

### Changed

- Updated to schema v0.8 and xml v0.4
- Export command does not write to stdout anymore,
  but writes the XML documents returned by xml.write to their respective file.

## [0.2.1] - 2021-01-27

### Fixed

- Respect --git-version option
- Match tags with and without v prefix

## [0.2.0] - 2021-01-26

### Changed

- Updated schema
- Export command writes to stdout

### Added

- Bundle command which reads catalog from multiple YAML or JSON files
  linked via $ref
- Option to infer and add catalog version from its git repo (bundle and export)

## [0.1.0] - 2020-12-22

### Added

- validate command to validate catalog files
- export command which writes xml files derived from catalog files

[Unreleased]: https://gitlab.com/rdmo-json/cli/compare/v0.5.1...main
[0.5.1]: https://gitlab.com/rdmo-json/cli/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/rdmo-json/cli/compare/v0.4.0...v0.5.0
[0.4.0]: https://gitlab.com/rdmo-json/cli/compare/v0.3.1...v0.4.0
[0.3.1]: https://gitlab.com/rdmo-json/cli/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/rdmo-json/cli/compare/v0.2.1...v0.3.0
[0.2.1]: https://gitlab.com/rdmo-json/cli/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/rdmo-json/cli/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/rdmo-json/cli/tags/v0.1.0

<!-- markdownlint-configure-file { "MD024": { "siblings_only": true }} -->
